import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularpractice';
  appname ='John';

  getname() {
    return this.appname;
  }
  obj = {
    name: 'mike',
    age: 21,
  }
  arr=['math', 'Science', 'english'];
  siteUrl=window.location.href;
}
